fn main() -> Result<(), Box<dyn std::error::Error>> {
    let nsi = nsi::Context::new(&[]).expect("Could no create ɴsɪ context.");

    // Camera transform
    nsi.create("cam_trs", "transform", &[]);
    let cam = [
        1.0f64, 0., 0., 0., 0., 1., 0., 0., 0., 0., -1., 0., 0., 0., 0., 1.,
    ];

    nsi.set_attribute(
        "cam_trs",
        &[nsi::double_matrix!("transformationmatrix", &cam)],
    );
    nsi.connect("cam_trs", "", nsi::NodeType::Root, "objects", &[]);

    // Perspective Camera Parameters
    nsi.create("maincamera", "perspectivecamera", &[]);
    nsi.set_attribute("maincamera", &[nsi::float!("fov", 100.)]);
    nsi.connect("maincamera", "", "cam_trs", "objects", &[]);

    // Camera's screen
    nsi.create("raster", "screen", &[]);
    nsi.set_attribute("raster", &[nsi::integer!("oversampling", 8)]);
    nsi.connect("raster", "", "maincamera", "screens", &[]);

    // Output drivers
    nsi.create("driver", "outputdriver", &[]);
    nsi.create("driver2", "outputdriver", &[]);
    nsi.set_attribute(
        "driver",
        &[
            nsi::string!("drivername", "idisplay"),
            nsi::string!("imagefilename", "area_lights"),
        ],
    );

    nsi.set_attribute(
        "driver2",
        &[
            nsi::string!("drivername", "exr"),
            nsi::string!("imagefilename", "area_lights.exr"),
        ],
    );

    // The layer (data types, naming, etc)
    nsi.create("beautylayer", "outputlayer", &[]);
    nsi.create("zbuffer", "outputlayer", &[]);
    nsi.set_attribute(
        "beautylayer",
        &[
            nsi::string!("variablename", "Ci"),
            nsi::string!("channelname", "beauty"),
            nsi::string!("scalarformat", "half"),
            nsi::integer!("withalpha", 1),
        ],
    );

    nsi.set_attribute(
        "zbuffer",
        &[
            nsi::string!("variablename", "z"),
            nsi::string!("variablesource", "builtin"),
            nsi::string!("layertype", "scalar"),
            nsi::string!("channelname", "z depth"),
            nsi::string!("scalarformat", "float"),
        ],
    );

    nsi.connect("beautylayer", "", "raster", "outputlayers", &[]);
    nsi.connect("zbuffer", "", "raster", "outputlayers", &[]);

    nsi.connect("driver", "", "beautylayer", "outputdrivers", &[]);
    nsi.connect("driver2", "", "beautylayer", "outputdrivers", &[]);

    nsi.connect("driver", "", "zbuffer", "outputdrivers", &[]);
    nsi.connect("driver2", "", "zbuffer", "outputdrivers", &[]);

    // Main scene transform
    let anchor = [
        1.0f64, 0., 0., 0., 0., 1., 0., 0., 0., 0., 1., 0., 0., 0., 2., 1.,
    ];

    nsi.create("anchor", "transform", &[]);
    nsi.set_attribute(
        "anchor",
        &[nsi::double_matrix!("transformationmatrix", &anchor)],
    );
    nsi.connect("anchor", "", ".root", "objects", &[]);

    // Red quads
    let nvertices_quads = [4, 4];
    let p = [
        -1.5f32, 0., 0.5, -0.5, 0., 0.5, -1.5, -1., 0.5, -0.5, -1., 0.5, 0.5, 0., 0.5, 1.5, 0.,
        0.5, 0.5, -1., 0.5, 1.5, -1., 0.5,
    ];
    let indices = [0, 1, 3, 2, 4, 5, 7, 6];

    // Passing a variable list of arguments
    let mut red_mesh_args = Vec::new();
    red_mesh_args.push(nsi::integers!("nvertices", &nvertices_quads));
    red_mesh_args.push(nsi::integers!("P.indices", &indices));
    red_mesh_args.push(nsi::points!("P", &p));

    nsi.create("red", "mesh", &[]);
    nsi.set_attribute("red", &red_mesh_args);
    nsi.connect("red", "", "anchor", "objects", &[]);

    // Blue quads, reuse nvertices and indices from red quads
    let p2 = [
        -1.3f32, -0.2, -0.8, -0.5, -0.2, -0.8, -1.3, -1., -0.8, -0.5, -1., -0.8, 0.5, -0.2, -0.8,
        1.3, -0.2, -0.8, 0.5, -1., -0.8, 1.3, -1., -0.8,
    ];

    nsi.create("blue", "mesh", &[]);
    nsi.set_attribute(
        "blue",
        &[
            nsi::integers!("nvertices", &nvertices_quads),
            nsi::integers!("P.indices", &indices),
            nsi::points!("P", &p2),
        ],
    );
    nsi.connect("blue", "", "anchor", "objects", &[]);

    // White light source (behind camera)
    nsi.create("white_area_light", "mesh", &[]);
    nsi.set_attribute(
        "white_area_light",
        &[
            nsi::integer!("nvertices", 4),
            nsi::points!(
                "P",
                &[-0.5f32, -0.5, -0.5, 0.5, -0.5, -0.5, 0.5, 0.5, -0.5, -0.5, 0.5, -0.5]
            ),
        ],
    );
    nsi.connect("white_area_light", "", ".root", "objects", &[]);

    // Top light – we ue a LUA script to make transforms
    let rotate = [90.0f32, 0., 0.];
    let scale = [0.1f32, 0.05, 1.];
    let translate = [0.0f32, 0.6, -1.25];

    nsi.evaluate(&[
        nsi::string!("type", "lua"),
        nsi::string!("filename", "../settransform.lua"),
        nsi::string!("handle", "green_light_trs"),
        nsi::vector!("rotate", &rotate),
        nsi::vector!("scale", &scale),
        nsi::vector!("translate", &translate),
    ]);
    nsi.connect("green_light_trs", "", "anchor", "objects", &[]);

    // Greenish light source (visible from camera)
    nsi.create("green_area_light", "mesh", &[]);
    nsi.set_attribute(
        "green_area_light",
        &[
            nsi::integer!("nvertices", 4),
            nsi::points!(
                "P",
                &[-1.0f32, -1., 0., 1., -1., 0., 1., 1., 0., -1., 1., 0.]
            ),
        ],
    );
    nsi.connect("green_area_light", "", "green_light_trs", "objects", &[]);

    // Attributes nodes. These are needed for shader assignments
    nsi.create("red_attr", "attributes", &[]);
    nsi.connect("red_attr", "", "red", "geometryattributes", &[]);
    nsi.create("blue_attr", "attributes", &[]);
    nsi.connect("blue_attr", "", "blue", "geometryattributes", &[]);
    nsi.create("white_attr", "attributes", &[]);
    nsi.connect(
        "white_attr",
        "",
        "white_area_light",
        "geometryattributes",
        &[],
    );
    nsi.create("green_attr", "attributes", &[]);
    nsi.connect(
        "green_attr",
        "",
        "green_area_light",
        "geometryattributes",
        &[],
    );

    // Red matte shader
    let red_color = [0.9f32, 0.1, 0.1];
    nsi.create("red_matte", "shader", &[]);
    nsi.set_attribute("red_matte", &[nsi::string!("shaderfilename", "../matte")]);
    nsi.set_attribute("red_matte", &[nsi::color!("Cs", &red_color)]);
    nsi.connect("red_matte", "", "red_attr", "surfaceshader", &[]);

    // Blue matte shader (different SetAttribute form than above, as an example)
    let blue_color = [0.2f32, 0.2, 0.9];
    nsi.create("blue_matte", "shader", &[]);
    nsi.set_attribute(
        "blue_matte",
        &[
            nsi::string!("shaderfilename", "../matte"),
            nsi::color!("Cs", &blue_color),
        ],
    );
    nsi.connect("blue_matte", "", "blue_attr", "surfaceshader", &[]);

    // White matte shader
    let white_color = [1.0f32, 1., 1.];
    nsi.create("white_matte", "shader", &[]);
    nsi.set_attribute(
        "white_matte",
        &[
            nsi::string!("shaderfilename", "../matte"),
            nsi::color!("Cs", &white_color),
        ],
    );

    // White light shader
    nsi.create("white_light", "shader", &[]);
    nsi.set_attribute(
        "white_light",
        &[
            nsi::string!("shaderfilename", "../emitter"),
            nsi::float!("power", 25.),
        ],
    );
    nsi.connect("white_light", "", "white_attr", "surfaceshader", &[]);

    // Green light shader
    let green_color = [0.2f32, 0.9, 0.3];
    nsi.create("green_light", "shader", &[]);
    nsi.set_attribute(
        "green_light",
        &[
            nsi::string!("shaderfilename", "../emitter"),
            nsi::float!("power", 1.8),
            nsi::color!("Cs", &green_color),
        ],
    );
    nsi.connect("green_light", "", "green_attr", "surfaceshader", &[]);

    // Floor
    nsi.create("floor", "mesh", &[]);
    nsi.set_attribute(
        "floor",
        &[
            nsi::integer!("nvertices", 4),
            nsi::points!(
                "P",
                &[-2.0f32, -1., 3., 2., -1., 3., 2., -1., 1., -2., -1., 1.]
            ),
        ],
    );

    nsi.connect("floor", "", ".root", "objects", &[]);
    nsi.create("floor_attributes", "attributes", &[]);
    nsi.connect("white_matte", "", "floor_attributes", "surfaceshader", &[]);
    nsi.connect("floor_attributes", "", "floor", "geometryattributes", &[]);

    // START RENDER
    nsi.render_control(&[nsi::string!("action", "start")]);
    nsi.render_control(&[nsi::string!("action", "wait")]);

    // good bye boss
    Ok(())
}
