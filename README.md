# NSI Examples

This repository contains commented examples of NSI scenes in various
programming languages.
## Hello Quads

This example contains an NSI scene that demonstrates some simple quad-based
geometry & lights in both `.nsi`, C++ and Rust formats. To render, compile the
shaders using `oscl`:
```
oslc emitter.osl
oslc matte.osl
```
After that you can render
* the `.nsi` file using:
  ```
  renderdl hello_quads.nsi
  ```
* the C++ program using:
  ```
  c++ hello_quads.c++ -I$DELIGHT/include -L$DELIGHT/lib -l3delight ; ./a.out
  ```
* the Rust program using:
  ```
  cd hello_quads.rust && cargo run && cd ..
  ```
