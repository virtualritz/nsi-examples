SetAttribute ".global" "lightcache" "int" 1 0

# Camera transform
Create "cam_trs" "transform"
SetAttribute "cam_trs"
  "transformationmatrix" "doublematrix" 1 [
  1 0 0 0
  0 1 0 0
  0 0 -1 0
  0 0 0 1 ]
Connect "cam_trs" "" ".root" "objects"

# Perspective Camera
Create "maincamera" "perspectivecamera"
SetAttribute "maincamera" "fov" "float" 1 100
Connect "maincamera" "" "cam_trs" "objects"

# Camera's screen
Create "raster" "screen"
SetAttribute "raster" "oversampling" "int" 1 8
Connect "raster" "" "maincamera" "screens"

# Output drivers
Create "exr" "outputdriver"
Create "tif" "outputdriver"

SetAttribute "exr"
	"drivername" "string" 1 ["exr"]
	"imagefilename" "string" 1 ["area_lights.exr"]
SetAttribute "tif"
	"drivername" "string" 1 ["tiff"]
	"imagefilename" "string" 1 ["area_lights.tif"]

# The layer (data types, naming, etc)
Create "beauty" "outputlayer"
SetAttribute "beauty"
	"variablename" "string" 1 "Ci"
	"channelname" "string" 1 "beauty"
	"scalarformat" "string" 1 "half"
	"withalpha" "string" 1 "half"
Connect "beauty" "" "raster" "outputlayers"

Connect "tif" "" "beauty" "outputdrivers"
#Connect "exr" "" "beauty" "outputdrivers"

# NSI main scene transform
Create "anchor" "transform"
SetAttribute "anchor"
  "transformationmatrix" "doublematrix" 1 [
  1 0 0 0
  0 1 0 0
  0 0 1 0
  0 0 2 1 ]
Connect "anchor" "" ".root" "objects"

# NSI red quads
Create "red" "mesh"
SetAttribute "red"
  "nvertices" "int" 2 [ 4 4 ]
  "P" "i point" 8 [
    -1.5 0 0.5  -0.5 0 0.5  -1.5 -1 0.5  -0.5 -1 0.5
    0.5 0 0.5  1.5 0 0.5  0.5 -1 0.5  1.5 -1 0.5]
  "P.indices" "int" 8 [ 0 1 3 2 4 5 7 6 ]
Connect "red" "" "anchor" "objects"

# NSI blue quads
Create "blue" "mesh"
SetAttribute "blue"
  "nvertices" "int" 2 [ 4 4 ]
  "P" "i point" 8 [
    -1.3 -0.2 -0.8  -0.5 -0.2 -0.8  -1.3 -1 -0.8  -0.5 -1 -0.8
    0.5 -0.2 -0.8  1.3 -0.2 -0.8  0.5 -1 -0.8  1.3 -1 -0.8]
  "P.indices" "int" 8 [ 0 1 3 2 4 5 7 6 ]
Connect "blue" "" "anchor" "objects"

# NSI white light source (behind camera)
Create "white_area_light" "mesh"
SetAttribute "white_area_light"
  "nvertices" "int" 1 [ 4 ]
  "P" "point" 4 [
    -0.5 -0.5 -0.5  0.5 -0.5 -0.5  0.5 0.5 -0.5  -0.5 0.5 -0.5 ]
Connect "white_area_light" "" ".root" "objects"

# NSI light transform in Lua
Evaluate
    "type" "string" 1 "lua"
    "filename" "string" 1 "${RIBDIR}/settransform.lua"
    "handle" "string" 1 "green_light_trs"
	"rotate" "vector" 1 [90 0 0]
	"scale" "vector" 1 [0.1 0.05 1]
	"translate" "vector" 1 [0 0.6 -1.25]
Connect "green_light_trs" "" "anchor" "objects"

# NSI greenish light source (visible from camera)
Create "green_area_light" "mesh"
SetAttribute "green_area_light"
  "nvertices" "int" 1 [ 4 ]
  "P" "point" 4 [
    -1 -1 0  1 -1 0  1 1 0  -1 1 0 ]
Connect "green_area_light" "" "green_light_trs" "objects"

# Attributes nodes
Create "red_attr" "attributes"
Connect "red_attr" "" "red" "geometryattributes"
Create "blue_attr" "attributes"
Connect "blue_attr" "" "blue" "geometryattributes"
Create "white_attr" "attributes"
Connect "white_attr" "" "white_area_light" "geometryattributes"
Create "green_attr" "attributes"
Connect "green_attr" "" "green_area_light" "geometryattributes"

# Red matte shader
Create "red_matte" "shader" 
SetAttribute "red_matte" "shaderfilename" "string" 1 "matte"
SetAttribute "red_matte" "Cs" "color" 1 [0.9 0.1 0.1]
Connect "red_matte" "" "red_attr" "surfaceshader"

# Blue matte shader
Create "blue_matte" "shader" 
SetAttribute "blue_matte" "shaderfilename" "string" 1 "matte"
SetAttribute "blue_matte" "Cs" "color" 1 [0.2 0.2 0.9]
Connect "blue_matte" "" "blue_attr" "surfaceshader"

# White matte shader
Create "white_matte" "shader" 
SetAttribute "white_matte" "shaderfilename" "string" 1 "matte"
SetAttribute "white_matte" "Cs" "color" 1 [1 1 1]

# White light shader
Create "white_light" "shader" 
SetAttribute "white_light" "shaderfilename" "string" 1 "emitter"
SetAttribute "white_light" "power" "float" 1 25
Connect "white_light" "" "white_attr" "surfaceshader"

# Green light shader
Create "green_light" "shader" 
SetAttribute "green_light" "shaderfilename" "string" 1 "emitter"
SetAttribute "green_light" "power" "float" 1 0.8
SetAttribute "green_light" "Cs" "color" 1 [0.2 0.9 0.3]
Connect "green_light" "" "green_attr" "surfaceshader"

# floor
Create "floor" "mesh"
SetAttribute "floor"
  "nvertices" "int" 1 [ 4 ]
	"P" "point" 4 [-2 -1 3  2 -1 3  2 -1 1  -2 -1 1]
Connect "floor" "" ".root" "objects"

Create "floor_attributes" "attributes"
Connect "white_matte" "" "floor_attributes" "surfaceshader"
Connect "floor_attributes" "" "floor" "geometryattributes"

RenderControl "action" "string" 1 "start"
